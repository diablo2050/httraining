#!/bin/bash
#trying out enviromental(internal)variables
#$# prints out the number of parameters given after the file name
echo $#


# $! process id of the last (right most for pipelines) command in the recently job put infto the background
echo $!

# $$ ID of the process that executed bash
echo $$

# $? exit status of the last command
echo $?

# $_ last field of the last command
echo $_

# $PATH PATH enviroment variable used to look-up executables
echo $PATH

# $OLDPWD previous working directory
echo $OLDPATH

# $FUNCNAME array of function names in the execution call stack
echo $FUNCNAME

# $BASH_SOURCE array containing source paths for elements in FUNCNAME array. can be used to get the script path.

echo $BASH_SOURCE

# $BASH_ALIASES associative array containing all currently defined aliases
echo $BASH_ALIASES

# $BASH_REMATCH array of matches from the last regex match
echo $BASH_REMATCH

# $BASH_VERSION bash string version
echo $BASH_VERSION

# $BASH_VERSIONINFO an array of 6 elements with Bash version information
echo $BASH_VERSIONINFO

# $BASH absolute path to the currently executing Bash shell iteself
echo $BASH

# $BASH_SUBSHELL bash subshell level
echo $BASH_SUBSHELL

# $ID real user ID of the process running bash
echo $ID

# $PS1 primary command line prompt
echo $PS1

# $PS2 secondary command line prompt
echo $PS2

# $PS3 tertiary command line prompt
echo $PS3

# $PS4 quanternary command line prompt
echo $PS4

# $RANDOM a pseudo random integer between 0 and 32767
echo $RANDOM

# $REPLY variable used by read by default when no variable is specified. also used by select to return the user-supplied value

# $PIPESTATUS array variable that holds the exit status values of each command in the most recently foreground pipeline.
echo $PIPESTATUS 

