#!/bin/bash
#By Amr Rashad on 14-11-2017
#For displaying IP and NS of Domains under /etc/userdomains For Cpanel servers.

domains=`cat /etc/userdomains | grep -v nobody | awk '{ print $1 }' | sed s'/.$//' `
printf  "%15s%24s%28s\n" "Host"  "IP" "NameServer"

for line in $domains
    do
        test=$(host $line)
        ip="$(grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' <<< "$test")"
        ns=$(host -t NS $line| grep -i "name server" | sed -n '1p' | awk '{ print $4}')
        if [ -z $ip ] || [ -z $ns ]
        then
            ip="Not Available"
            ns="Nost Available"
            printf "%-30s\t%s\t\t%s\n" "$line" "$ip" "$ns"
        else
            printf "%-30s\t%s\t\t%s\n" "$line" "$ip" "$ns"
        fi

    done