#!/bin/bash

#######################
#Works with this file format as an arg:
# cat /tmp/domains2.lst
# horizontechs.com:cnn.com:msn.com
#######################

#Setting vars
DOMAINS_FILE=$1

#Whois function
WhoIs() {
for DOMAIN in $(cat $1 | sed 's/\:/ /g')
do
echo -n "$DOMAIN Expiry date is ->"
whois $DOMAIN | grep "Registry Expiry Date" | cut -f2 -d\:
done
}

#Calling Function
WhoIs $DOMAINS_FILE