#!/bin/bash
> hosts.txt
> started.txt
> finished.txt
> result.txt

ansible all -m shell -a "sudo egrep '^201[0-9]|started|finished' /root/rsync.log" >> /root/done.txt
hosts=`cat /root/done.txt | egrep "*.orkiservers.com" | awk '{ print $1 }' >> hosts.txt`
started=`sed -n '/started/{n;p}' /root/done.txt >> started.txt`
finished=$(sed -n '/finished/{n;p}' /root/done.txt >> finished.txt)
pr -mts hosts.txt started.txt finished.txt >> result.txt
printf "%-37s%-17s%-25s\n" "Host" "started" "finished"

cat result.txt | column -t
