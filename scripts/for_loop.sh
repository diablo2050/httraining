#!/bin/bash
#testing for loop iteration

echo "please enter a directory to scan"
read path

if [ -d $path ]
then
	for path in `ls $path`
	do
	echo  "/$path"
	done
fi

#specifying a range to iterate from

echo "Numbers between 5 to 10 -"
for num in {5..10}
do
	echo -n "$num"
done

echo
echo "Odd numbers between 1 to 10 -"
for num in {1..10..2}
do
	echo -n "$num"
done
echo
