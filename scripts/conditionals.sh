#!/bin/bash
#testing for IF functionality

echo -n "Enter a file or directory "
read file
# -e flag tests whether a file exists or not (including directories)
# -f for filename
# -d for a directory
# -p for named pipe
# -S for a named socket
# -b for a block device
# -c for a character device
# -z for empty or contains only spaces
# -r for a readable file
# -w for a writeable file
# -x for an excutable file
# -n for string is not null

# integer comparison
# -eq equal to
# -ne not equal to
# -gt is greater than
# -ge is greater than or equal to
# -lt is less than
# -le is less than or equal to
# < is less than
# <= is less than or equal to
# > is greater than
# >= is greater than or equal to

# string comparizon
# = is equal to
# == same as equal to
# !# is not equal to
# -a logical AND
# -o logical OR 


if [[ -e $file ]]; then
	echo "$file exists"
fi
