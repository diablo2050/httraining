#!/bin/bash
#testing ssh connection
# Author: Amr Rashad
#using nmap to scan the network
echo "please wait while we scan the network"
nmap -sP 192.168.100.0/24  > /tmp/.hideme 
echo

#ensure that neither 0 nor 255 are entered by mistake
echo "Please enter the last octet of the ip"
read last
if [  $last -ge 1 ] && [ $last -le 254 ]
then
	echo "correct ip entered"
else
	echo "not found"
	exit
fi

# grep ip from file to check if it actually exists on the network and putting output to /dev/null because we want the exit status only
grep 192.168.100.$last /tmp/.hideme > /dev/null

if [ $? -eq 0 ]
then
	echo "found host and pinging now"
	ping -c 4 192.168.100.$last
elif [ $? -eq 1 ]
then
	echo "no host found"
fi
echo "enter username"
read user
#ssh into a system and collect system data and logs
ssh -t $user@192.168.100.$last "
touch /tmp/.amr
uptime > /tmp/.amr
free -h >> /tmp/.amr
lsblk >> /tmp/.amr
lscpu >> /tmp/.amr
#tar -cf  /tmp/.amrlogs.tar /var/log/*
scp /tmp/.amr* arashad@192.168.100.103:/home/arashad 
"
