!/bin/bash

#################
#Works with this file format as arg:
#cat /tmp/domains.lst
# horizontechs.com
# cnn.com
# msn.com
# alwafd.org
# fsf.org
# gnu.org
#################

#Setting vars
DOMAINS_FILE=$1
#Whois function
WhoIs() {
for DOMAIN in $(cat $1)
do
echo -n "$DOMAIN Expiry date is ->"
whois $DOMAIN | grep "Registry Expiry Date" | cut -f2 -d\:
done
}

#Calling Function
WhoIs $DOMAINS_FILE $ONLY_IF