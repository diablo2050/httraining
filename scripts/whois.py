#!/usr/bin/env  python

from __future__ import print_function
import pythonwhois
from sys import argv

if len(argv) < 2:
    print("usage is ./whois.py /path/filename")
    exit()
elif len(argv) > 2:
    print("too many arguments")
    exit()
domains = 'domains.txt'
with open(domains) as d:
    for line in d :
         one=pythonwhois.get_whois(line)
         print(line.strip(), ':', one['expiration_date'][0])
