#!/bin/bash
#different operations on indexed arrays

declare -a countries=(India Japan Indonesia 'Sri Lanka' USA Canada)
#we use # with @ to list contents as numbers
echo "Length of array countries = ${#countries[@]}"
echo ${countries[@]}

#deleting second element of the array using unset
unset countries[1]
echo "Length after deleting second element ${#countries[@]}"
echo ${countries[@]}

#adding two more countries to array
countries=("${countries[@]}" "England" "France")

echo "Length after adding 2 more items ${#countries[@]}"
echo ${countries[@]}
